package com.example.TeslaModelRCApp;

import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.net.wifi.WifiConfiguration;
import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;
import android.os.IBinder;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;

import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.example.TeslaModelRCApp.Adapter.FragmentAdapter;

import static java.lang.Math.abs;

public class MainActivity extends AppCompatActivity {

    private static final String TAG = "MainActivity";
    private static MainActivity instance;


    //Services
    Services mService;
    Boolean mIsBound = false;
    public static int fake_sensor = 0;

    //Fragments
    ViewPager viewPager;
    TabLayout tabLayout;

    //Wifi
    WifiManager wifiManager;
    public boolean wifiConnected = false;
    WifiConfiguration wifiConfig = new WifiConfiguration();
    String SSID = "tesla_model_rc";
    String WPA2 = "passpass";

    public TestClient coapClient = new TestClient();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        instance = this;

        setContentView(R.layout.activity_main);

        wifiManager = (WifiManager) getApplicationContext().getSystemService(Context.WIFI_SERVICE);
//        wifiManager.setWifiEnabled(false);

        viewPager = findViewById(R.id.viewPager);
        tabLayout = findViewById(R.id.tabLayout);

        FragmentAdapter fragmentAdapter = new FragmentAdapter(getSupportFragmentManager(), this);
        viewPager.setAdapter(fragmentAdapter);
        tabLayout.setupWithViewPager(viewPager);

//        wifiManager.setWifiEnabled(true);
//        connectWifi(wifiConfig,wifiManager,SSID,WPA2);
        coapClient.start();
    }

    public void connectWifi(WifiConfiguration wifiConfig, WifiManager wifiManager, String SSID, String WPA2){
        wifiConfig.SSID = "\"" + SSID + "\"";
        wifiConfig.preSharedKey = "\"" + WPA2 + "\"";
        int netId = wifiManager.addNetwork(wifiConfig);
        wifiManager.enableNetwork(netId, true);
        wifiManager.disconnect();
        boolean isConnectionSuccessful = wifiManager.reconnect();
        WifiInfo info = wifiManager.getConnectionInfo ();
        String ssid  = info.getSSID();
        Toast.makeText(MainActivity.this, "Connected to: tesla_model_rc" ,Toast.LENGTH_LONG).show();

    }

    public static MainActivity getInstance() {
        return instance;
    }

    @Override
    protected void onResume() {
        super.onResume();
        startService();
    }

    @Override
    protected void onStop() {
        Log.d(TAG, "Service Stop CALLED");
        super.onStop();
        if(mIsBound){
            unbindService(serviceConnection);
            mIsBound = false;
        }
    }

    private void startService(){
        Log.d(TAG, "Starting Service called");
        Intent serviceIntent = new Intent(this, Services.class);
        startService(serviceIntent);

        bindService();
    }

    private void bindService(){
        Log.d(TAG, "Binding Service called");
        Intent serviceBindIntent =  new Intent(this, Services.class);
        bindService(serviceBindIntent, serviceConnection, Context.BIND_AUTO_CREATE);
    }

    private ServiceConnection serviceConnection = new ServiceConnection() {
        @Override
        public void onServiceConnected(ComponentName className, IBinder iBinder) {
            Log.d(TAG, "ServiceConnection: connected to service.");
            // We've bound to MyService, cast the IBinder and get MyBinder instance
            Services.MyBinder binder = (Services.MyBinder) iBinder;
            mService = binder.getService();
            mIsBound = true;
        }
        @Override
        public void onServiceDisconnected(ComponentName arg0) {
            Log.d(TAG, "ServiceConnection: disconnected from service.");
            mIsBound = false;
        }
    };

    public void clearCoordinates(View v){
        mapfragment.getINSTANCE().map.clear();
        mapfragment.getINSTANCE().destination = null;
        mapfragment.getINSTANCE().destinationSet = false;
    }

    public void sendStart(View v){ coapClient.sendPut("/setStatus","start");}

    public void sendStop(View v){ coapClient.sendPut("/setStatus","stop");}

    public void sendDestination(View v){
        if(mapfragment.getINSTANCE().destination == null){
            Toast.makeText(MainActivity.this, "Please pick a destination!" ,Toast.LENGTH_LONG).show();
            return;
        }
        double tempLatitude = mapfragment.getINSTANCE().destination.latitude;
        double tempLongitude = mapfragment.getINSTANCE().destination.longitude;

        boolean latititudeIsNegative = tempLatitude < 0 ? true : false;
        boolean longitudeIsNegative = tempLongitude < 0 ? true : false;

        String Lat = latititudeIsNegative ? String.format("%010.6f" ,abs(tempLatitude)) : String.format("%010.6f" ,tempLatitude);
        String Long = longitudeIsNegative ? String.format("%010.6f" ,abs(tempLongitude)) : String.format("%010.6f" ,tempLongitude);

        Lat = appendSign(latititudeIsNegative, Lat);
        Long = appendSign(longitudeIsNegative, Long);
        String stringlatlong = Lat + "," + Long;

        coapClient.sendPut("/setCoordinates", stringlatlong);
    }
    public String appendSign(boolean isNegative, String valueAsAscii){
        return valueAsAscii = (isNegative) ? "-" + valueAsAscii : "+" + valueAsAscii;
    }


}

